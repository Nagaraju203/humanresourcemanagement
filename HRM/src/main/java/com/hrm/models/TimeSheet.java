package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class TimeSheet {
	
	@Id
	@GeneratedValue
	private Integer timeSheetID;
	private String date;
	private String inTime;
	private String outTime;
	private String noOfHours;
	private String process;
	private String subProcess;
	
	@OneToMany
	private Employee employeeID;
	
	private Week weekID;

	public Integer getTimeSheetID() {
		return timeSheetID;
	}

	public void setTimeSheetID(Integer timeSheetID) {
		this.timeSheetID = timeSheetID;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getInTime() {
		return inTime;
	}

	public void setInTime(String inTime) {
		this.inTime = inTime;
	}

	public String getOutTime() {
		return outTime;
	}

	public void setOutTime(String outTime) {
		this.outTime = outTime;
	}

	public String getNoOfHours() {
		return noOfHours;
	}

	public void setNoOfHours(String noOfHours) {
		this.noOfHours = noOfHours;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}

	public String getSubProcess() {
		return subProcess;
	}

	public void setSubProcess(String subProcess) {
		this.subProcess = subProcess;
	}

	public Employee getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(Employee employeeID) {
		this.employeeID = employeeID;
	}

	public Week getWeekID() {
		return weekID;
	}

	public void setWeekID(Week weekID) {
		this.weekID = weekID;
	}
	
	
	
	
	

}
