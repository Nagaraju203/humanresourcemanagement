package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class ResignationInfo {
	
	@Id
	@GeneratedValue
	private Integer resignationID;
	private String reason;
	private String lastWorkingDay;
	private Projects projects;
	private String resignationLetter;

	@OneToOne
	@JoinColumn(name="empid_fk")
	private Employee employee;
	
	@OneToOne(mappedBy="resigInfo")
	private NoObjectionCerificate noc;
	
	
	
	
	public NoObjectionCerificate getNoc() {
		return noc;
	}
	public void setNoc(NoObjectionCerificate noc) {
		this.noc = noc;
	}
	public Integer getResignationID() {
		return resignationID;
	}
	public void setResignationID(Integer resignationID) {
		this.resignationID = resignationID;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public String getLastWorkingDay() {
		return lastWorkingDay;
	}
	public void setLastWorkingDay(String lastWorkingDay) {
		this.lastWorkingDay = lastWorkingDay;
	}
	public Projects getProjects() {
		return projects;
	}
	public void setProjects(Projects projects) {
		this.projects = projects;
	}
	public String getResignationLetter() {
		return resignationLetter;
	}
	public void setResignationLetter(String resignationLetter) {
		this.resignationLetter = resignationLetter;
	}
	
	

}
