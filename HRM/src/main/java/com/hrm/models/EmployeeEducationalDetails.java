package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeEducationalDetails {
	@Id
	@GeneratedValue
	private Integer educationalID;
	
	private String ssc;
	private String intermideate;
	private String graduation;
	public Integer getEducationalID() {
		return educationalID;
	}
	public void setEducationalID(Integer educationalID) {
		this.educationalID = educationalID;
	}
	public String getSsc() {
		return ssc;
	}
	public void setSsc(String ssc) {
		this.ssc = ssc;
	}
	public String getIntermideate() {
		return intermideate;
	}
	public void setIntermideate(String intermideate) {
		this.intermideate = intermideate;
	}
	public String getGraduation() {
		return graduation;
	}
	public void setGraduation(String graduation) {
		this.graduation = graduation;
	}
	@Override
	public String toString() {
		return "EmployeeEducationalDetails [educationalID=" + educationalID + ", ssc=" + ssc + ", intermideate="
				+ intermideate + ", graduation=" + graduation + "]";
	}
	
	

}
