package com.hrm.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeePersonalDetails {
	@Id
	@GeneratedValue
	private Integer ID;
	private String firstName;
	private String lastName;
	private String gender;
	private String MaterialStatus;
	private String bloodGroup;
	private String fatherName;
	
	private List<Address> listOfAddress;
	
	private List<Mobile> listOfMobiles;

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaterialStatus() {
		return MaterialStatus;
	}

	public void setMaterialStatus(String materialStatus) {
		MaterialStatus = materialStatus;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public List<Address> getListOfAddress() {
		return listOfAddress;
	}

	public void setListOfAddress(List<Address> listOfAddress) {
		this.listOfAddress = listOfAddress;
	}

	public List<Mobile> getListOfMobiles() {
		return listOfMobiles;
	}

	public void setListOfMobiles(List<Mobile> listOfMobiles) {
		this.listOfMobiles = listOfMobiles;
	}

	@Override
	public String toString() {
		return "EmployeePersonalDetails [ID=" + ID + ", firstName=" + firstName + ", lastName=" + lastName + ", gender="
				+ gender + ", MaterialStatus=" + MaterialStatus + ", bloodGroup=" + bloodGroup + ", fatherName="
				+ fatherName + ", listOfAddress=" + listOfAddress + ", listOfMobiles=" + listOfMobiles + "]";
	}
	
	
	
}
