package com.hrm.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name= "leavestatus")
public class LeaveStatus {
	@Id
	@GeneratedValue
	@Column(name="lsid")
	private Integer leaveStatusID;
	
	@Column(name="leavefromdate")
	private String fromDate;
	
	private String toDate;
	private String leaveStatus;
	
	private String reason;
	@OneToOne
	@JoinColumn(name="leaveid_fk")
	private Leave leaveID;
	
	@OneToOne
	@JoinColumn(name="empid_fk")
	private Employee employeeID;
	
	@OneToOne
	@JoinColumn(name="approvedmanager_id_fk")
	private Employee approvalManagerID;
	
	
	
	public Integer getLeaveStatusID() {
		return leaveStatusID;
	}
	public void setLeaveStatusID(Integer leaveStatusID) {
		this.leaveStatusID = leaveStatusID;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getLeaveStatus() {
		return leaveStatus;
	}
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Leave getLeaveID() {
		return leaveID;
	}
	public void setLeaveID(Leave leaveID) {
		this.leaveID = leaveID;
	}
	public Employee getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(Employee employeeID) {
		this.employeeID = employeeID;
	}
	public Employee getApprovalManagerID() {
		return approvalManagerID;
	}
	public void setApprovalManagerID(Employee approvalManagerID) {
		this.approvalManagerID = approvalManagerID;
	}
	@Override
	public String toString() {
		return "LeaveStatus [leaveStatusID=" + leaveStatusID + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", leaveStatus=" + leaveStatus + ", reason=" + reason + ", leaveID=" + leaveID + ", employeeID="
				+ employeeID + ", approvalManagerID=" + approvalManagerID + "]";
	}
	
	

}
