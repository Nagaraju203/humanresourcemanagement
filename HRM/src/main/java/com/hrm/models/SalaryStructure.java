package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SalaryStructure {
	@Id
	@GeneratedValue
	private Integer ssid;
	private Integer basic;
	private Integer hra;
	private Integer totalAllowances;
	private Integer pf;
	
	private Integer pTax;
	private Integer insurance;
	
	
	
	
	
	public Integer getSsid() {
		return ssid;
	}
	public void setSsid(Integer ssid) {
		this.ssid = ssid;
	}
	public Integer getBasic() {
		return basic;
	}
	public void setBasic(Integer basic) {
		this.basic = basic;
	}
	public Integer getHra() {
		return hra;
	}
	public void setHra(Integer hra) {
		this.hra = hra;
	}
	public Integer getTotalAllowances() {
		return totalAllowances;
	}
	public void setTotalAllowances(Integer totalAllowances) {
		this.totalAllowances = totalAllowances;
	}
	public Integer getPf() {
		return pf;
	}
	public void setPf(Integer pf) {
		this.pf = pf;
	}
	public Integer getpTax() {
		return pTax;
	}
	public void setpTax(Integer pTax) {
		this.pTax = pTax;
	}
	public Integer getInsurance() {
		return insurance;
	}
	public void setInsurance(Integer insurance) {
		this.insurance = insurance;
	}
	
	
	
}
