package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Fresher {
	@Id
	@GeneratedValue
	private Integer fresherID;
	private Integer yearOfPassOut;
	private String qualification;
	private Double percentage;
	
	@OneToOne
	@JoinColumn(name="cidfk")
	private Candidate candidate;

	public Integer getFresherID() {
		return fresherID;
	}

	public void setFresherID(Integer fresherID) {
		this.fresherID = fresherID;
	}

	public Integer getYearOfPassOut() {
		return yearOfPassOut;
	}

	public void setYearOfPassOut(Integer yearOfPassOut) {
		this.yearOfPassOut = yearOfPassOut;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	@Override
	public String toString() {
		return "Fresher [fresherID=" + fresherID + ", yearOfPassOut=" + yearOfPassOut + ", qualification="
				+ qualification + ", percentage=" + percentage + ", candidate=" + candidate + "]";
	}
	
	
	
}
