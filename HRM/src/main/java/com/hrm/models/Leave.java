package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Leave {
	@Id
	@GeneratedValue
	private Integer leaveID;
	private String typeOfLeave;
	private Double noOfLeaves;
	
	@OneToOne(mappedBy="leaveID")
	private LeaveStatus leaveStatus;
	
	
	public LeaveStatus getLeaveStatus() {
		return leaveStatus;
	}
	public void setLeaveStatus(LeaveStatus leaveStatus) {
		this.leaveStatus = leaveStatus;
	}
	public Integer getLeaveID() {
		return leaveID;
	}
	public void setLeaveID(Integer leaveID) {
		this.leaveID = leaveID;
	}
	public String getTypeOfLeave() {
		return typeOfLeave;
	}
	public void setTypeOfLeave(String typeOfLeave) {
		this.typeOfLeave = typeOfLeave;
	}
	public Double getNoOfLeaves() {
		return noOfLeaves;
	}
	public void setNoOfLeaves(Double noOfLeaves) {
		this.noOfLeaves = noOfLeaves;
	}
	@Override
	public String toString() {
		return "Leave [leaveID=" + leaveID + ", typeOfLeave=" + typeOfLeave + ", noOfLeaves=" + noOfLeaves + "]";
	}
	
	

}
