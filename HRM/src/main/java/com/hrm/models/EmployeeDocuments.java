package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeDocuments {
	@Id
	@GeneratedValue
	private Integer id;
	
	private EmployeePreviousOrg organizationID;
	
	private EmployeeEducationalDetails educationalDetailsID;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmployeePreviousOrg getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(EmployeePreviousOrg organizationID) {
		this.organizationID = organizationID;
	}

	public EmployeeEducationalDetails getEducationalDetailsID() {
		return educationalDetailsID;
	}

	public void setEducationalDetailsID(EmployeeEducationalDetails educationalDetailsID) {
		this.educationalDetailsID = educationalDetailsID;
	}

	@Override
	public String toString() {
		return "EmployeeDocuments [id=" + id + ", organizationID=" + organizationID + ", educationalDetailsID="
				+ educationalDetailsID + "]";
	}
	
	

}
