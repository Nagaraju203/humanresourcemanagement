package com.hrm.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class InterviewManagerRound2 {
	@Id
	@GeneratedValue
	private Integer round2ID;
	private Integer rating;
	
	private Employee managerName;

	
	private String round2ManagerStatus;
	

	private List<Technologies> listOfTechnologies;
	@OneToOne
	@JoinColumn(name="cidfk")
	private Candidate candidate;
	

	public Integer getRound2ID() {
		return round2ID;
	}

	public void setRound2ID(Integer round2id) {
		round2ID = round2id;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public Employee getManagerName() {
		return managerName;
	}

	public void setManagerName(Employee managerName) {
		this.managerName = managerName;
	}

	public String getRound2ManagerStatus() {
		return round2ManagerStatus;
	}

	public void setRound2ManagerStatus(String round2ManagerStatus) {
		this.round2ManagerStatus = round2ManagerStatus;
	}

	public Candidate getCandidate() {
		return candidate;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public List<Technologies> getListOfTechnologies() {
		return listOfTechnologies;
	}

	public void setListOfTechnologies(List<Technologies> listOfTechnologies) {
		this.listOfTechnologies = listOfTechnologies;
	}

	@Override
	public String toString() {
		return "InterviewManagerRound2 [round2ID=" + round2ID + ", rating=" + rating + ", managerName=" + managerName
				+ ", round2ManagerStatus=" + round2ManagerStatus + ", candidate=" + candidate + ", listOfTechnologies="
				+ listOfTechnologies + "]";
	}
	
	

}
