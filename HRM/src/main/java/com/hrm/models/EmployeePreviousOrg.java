package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeePreviousOrg {
	@Id
	@GeneratedValue
	private Integer prevOrgid;
	
	private String organizationName;
	
	private String designation;
	
	private String releavingLetter;
	
	private String incrementLetter;
	private String experienceLetter;
	
	private String payslip;

	public Integer getPrevOrgid() {
		return prevOrgid;
	}

	public void setPrevOrgid(Integer prevOrgid) {
		this.prevOrgid = prevOrgid;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getReleavingLetter() {
		return releavingLetter;
	}

	public void setReleavingLetter(String releavingLetter) {
		this.releavingLetter = releavingLetter;
	}

	public String getIncrementLetter() {
		return incrementLetter;
	}

	public void setIncrementLetter(String incrementLetter) {
		this.incrementLetter = incrementLetter;
	}

	public String getExperienceLetter() {
		return experienceLetter;
	}

	public void setExperienceLetter(String experienceLetter) {
		this.experienceLetter = experienceLetter;
	}

	public String getPayslip() {
		return payslip;
	}

	public void setPayslip(String payslip) {
		this.payslip = payslip;
	}

	@Override
	public String toString() {
		return "EmployeePreviousOrg [prevOrgid=" + prevOrgid + ", organizationName=" + organizationName
				+ ", designation=" + designation + ", releavingLetter=" + releavingLetter + ", incrementLetter="
				+ incrementLetter + ", experienceLetter=" + experienceLetter + ", payslip=" + payslip + "]";
	}
	
	

}
