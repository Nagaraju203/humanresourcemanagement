package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Technologies {
	@Id
	@GeneratedValue
	private Integer technologyID;
	private String technologyName;
	
	
	@ManyToMany
	@JoinTable(name="candidate_Technologies", 
	joinColumns={@JoinColumn(name="fktid")},
	inverseJoinColumns= {@JoinColumn(name="fkcid")})
	private Candidate candidate;
	
	
	@ManyToOne
	@JoinColumn(name="techid")
	private InterviewManagerRound2 interviewmanager2;
	
	
	public Integer getTechnologyID() {
		return technologyID;
	}
	public void setTechnologyID(Integer technologyID) {
		this.technologyID = technologyID;
	}
	public String getTechnologyName() {
		return technologyName;
	}
	public void setTechnologyName(String technologyName) {
		this.technologyName = technologyName;
	}
	

}
