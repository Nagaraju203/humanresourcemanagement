package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class InterviewProcessStatus {
	@Id
	@GeneratedValue
	private Integer statusID;
	private String name;
	public Integer getStatusID() {
		return statusID;
	}
	public void setStatusID(Integer statusID) {
		this.statusID = statusID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "InterviewProcessStatus [statusID=" + statusID + ", name=" + name + "]";
	}
	
	//why this class created???
	
	

}
