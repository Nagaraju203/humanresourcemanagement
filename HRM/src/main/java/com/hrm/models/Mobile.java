package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Mobile {
	@Id
	@GeneratedValue
	private Integer mobileID;
	private Long mobileNumber;
	public Integer getMobileID() {
		return mobileID;
	}
	public void setMobileID(Integer mobileID) {
		this.mobileID = mobileID;
	}
	public Long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	

}
