package com.hrm.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Qualification {
	@Id
	@GeneratedValue
	private Integer qualificationID;
	private String qualificationName;

	
	
	
	@OneToOne
	@JoinColumn(name="empqual_fkid")
	private Employee emploee;
	
	
	
	
	
	
	
	public Integer getQualificationID() {
		return qualificationID;
	}
	public void setQualificationID(Integer qualificationID) {
		this.qualificationID = qualificationID;
	}
	public String getQualificationName() {
		return qualificationName;
	}
	public void setQualificationName(String qualificationName) {
		this.qualificationName = qualificationName;
	}
	
	

}
